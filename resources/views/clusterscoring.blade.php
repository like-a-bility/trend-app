@extends('app')

@section('title', 'Cluster Ranking Score')

@section('content')
    <div class="row mb-2">
        <div class="col-md-12 text-center">
            <h2>{{ $text }}</h2>
        </div>
        <div class="col-md-12">
            <div class="card mt-2">
                <div class="card-body">
                    <h3>Trendometro</h3>
                    <p>Score <b>{{ round($score, 3) }}</b></p>
                    <div class="progress" style="height: 2rem;">
                        <div class="progress-bar bg-success" role="progressbar" style="width:{{ round($score, 2) }}%;text-align: right;font-size: 28px;" aria-valuenow="{{ round($score, 2) }}" aria-valuemin="0" aria-valuemax="100">{{ ($progressTrend > 0) ? '⇨' : (($progressTrend == 0) ? '=' : '⇦') }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div style="display:inline-block; width:100%; overflow-y:auto; margin:0;">
                        <div class="slider">
                            <div>
                                @foreach($clouds as $cluster => $c)
                                    <div class="point" style="left:{{ 100 - ((($ranks[$cluster]-$min) * 100)/ ($max-$min)) }}%;">
                                        {{ round($ranks[$cluster], 3) }}
                                        <div class="popup">
                                            <div id="tagcloud_{{$cluster}}"></div>
                                            <script>
                                                $('#tagcloud_{{$cluster}}').jQCloud({!! json_encode($c) !!}, {
                                                    height: 225
                                                });
                                            </script>
                                        </div>
                                    </div>
                                @endforeach
                                <div class="point current" style="left:{{ 100 - ((($distance-$min) * 100)/ ($max-$min)) }}%;">
                                    {{ round($distance, 3) }}
                                    <div class="popup">
                                        {{ $text }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h3 class="mt-3">Top cluster n.{{ $fallInto }} tweets</h3>
        </div>
        @foreach($examples as $ex)
            <div class="col-md-3">
                <div class="card mt-3">
                    <div class="card-body">
                        {{ $ex->text }}
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection

@section('head')
    <style>
        .slider {
            position: relative;
            height: 40px;
            border-radius: 10px;
            margin-right: 150px;
            margin-left: 125px;
            margin-top: 280px;
            margin-bottom: 50px;
        }

        .slider > div {
            position: absolute;
            left: 13px;
            right: 15px;
            height: 30px;
            width: 200%;
        }

        .slider > div {
            position: absolute;
            left: 0;
            height: 100%;
            border-radius: 14px;
            background: rgb(255, 0, 0);
            background: linear-gradient(90deg, rgba(255, 0, 0, 1) 0%, rgba(255, 179, 92, 1) 30%, rgba(255, 218, 92, 1) 65%, rgba(92, 249, 255, 1) 100%);
        }

        .slider > div > .point {
            position: absolute;
            top: -10px;
            z-index: 2;
            height: 60px;
            width: 60px;
            text-align: center;
            box-shadow: 0 3px 8px rgba(0, 0, 0, 0.4);
            background-color: #FFF;
            border-radius: 50%;
            display: flex;
            align-items: center;
            justify-content: center;
        }

        .slider > div > .point:hover {
            z-index: 555555 !important;
        }

        .slider > div > .current {
            background-color: #ff0000 !important;
            z-index: 3;
            height: 50px;
            width: 50px;
            border-radius: 0%;
            top: -5px;
        }

        .slider .popup {
            width: 300px;
            background-color: #FFF;
            text-align: center;
            border-radius: 6px;
            border: 2px solid #000;
            padding: 8px 0;
            position: absolute;
            z-index: 1;
            bottom: 125%;
            left: 50%;
            margin-left: -150px;
        }

        .slider .popup::after {
            content: "";
            position: absolute;
            top: 100%;
            left: 50%;
            margin-left: -10px;
            border-width: 10px;
            border-style: solid;
            border-color: #000 transparent transparent transparent;
        }
    </style>
@endsection