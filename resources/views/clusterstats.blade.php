@extends('app')

@section('title', 'Cluster Stats')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card mb-3">
                <div class="card-body">
                    <form method="post" action="{{ route('clustering.search') }}">
                        @csrf

                        <input type="text" name="phrase" class="form-control">

                        <button class="btn btn-primary mt-1" type="submit">Cerca</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @foreach($charts as $index => $chart)
        <div class="row justify-content-center">
            <div class="col-md-4">
                <div class="card mb-3">
                    <div class="card-body">
                        <canvas id="chartRankPerDay_{{ $index }}" height="300"></canvas>
                        <script>
                            new Chart($('#chartRankPerDay_{{$index}}'), {
                                type: 'line',
                                data: {
                                    labels: {!! json_encode(array_keys($chart[0])) !!},
                                    datasets: [{
                                        label: 'Cluster n. {{ $index }} - Mean Rank',
                                        data: {!! json_encode(array_values($chart[0])) !!},
                                        backgroundColor: "rgba(153,255,51,0.4)"
                                    }]
                                }
                            });
                        </script>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card mb-3">
                    <div class="card-body">
                        <canvas id="sizeOverTime_{{ $index }}" height="300"></canvas>
                        <script>
                            new Chart($('#sizeOverTime_{{$index}}'), {
                                type: 'line',
                                data: {
                                    labels: {!! json_encode(array_keys($chart[2])) !!},
                                    datasets: [{
                                        label: 'Cluster n. {{ $index }} - Size Over Time',
                                        data: {!! json_encode(array_values($chart[2])) !!},
                                        backgroundColor: "rgba(255,102,51,0.4)"
                                    }]
                                }
                            });
                        </script>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card mb-3">
                    <div class="card-body">
                        <div id="tagcloud_{{$index}}"></div>
                        <script>
                            $('#tagcloud_{{$index}}').jQCloud({!! json_encode($chart[1]) !!}, {
                                height: 367
                            });
                        </script>
                    </div>
                </div>
            </div>
        </div>
        <hr>
    @endforeach
@endsection
