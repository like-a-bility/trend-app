@extends('app')

@section('title', 'Result')

@section('content')
    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <img class="card-img-top" src="{{ $image }}">
                <div class="card-body">
                    <p class="card-text">{{ $caption }}</p>
                </div>
            </div>
            <div class="card mt-3">
                <div class="card-body" style="height: 350px; overflow-y: scroll; overflow-x: scroll; padding: 0">
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">Hashtag</th>
                            <th scope="col">Usages</th>
                            <th scope="col">Likes</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($query->groupBy('hashtag') as $hashtag => $data)
                            <tr>
                                <td>{{ $hashtag }}</td>
                                <td>{{ $data->first()->n_hashtag }}</td>
                                <td>{{ $data->first()->total_likes }}</td>
                            </tr>
                            @continue
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <div id="mapid"></div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            var map = L.map('mapid', {
                center: [53.27835301753182, 32.738089969339505],
                minZoom: 2,
                zoom: 4
            });

            L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
                subdomains: ['a', 'b', 'c']
            }).addTo(map);
                    @foreach($query->groupBy(['country', 'city']) as $country => $cities)
                        @foreach($cities as $city => $tags)
                            var icon = L.divIcon({
                                    className: "my-custom-pin",
                                    iconAnchor: [0, 24],
                                    labelAnchor: [-6, 0],
                                    popupAnchor: [0, -36],
                                    html: '<span style="background-color: rgb({{ intval(($tags->sum('city_total_likes') / $max_likes_cities) * 255) }},0,{{ intval(255 -(($tags->sum('city_total_likes') / $max_likes_cities) * 255)) }}); width: 2rem; height: 2rem; display: block; left: -1rem; top: -0.9rem; position: relative; border-radius: 3rem 3rem 0; transform: rotate(45deg); border: 1px solid #FFFFFF">'
                                });

                            L.marker([{{ $tags->first()->latitude }}, {{ $tags->first()->longitude }}], {
                                title: '{{ $city }}, {{ $country }}',
                                icon: icon
                            }).bindPopup('<b>{{ $city }}, {{ $country }}</b><br>Likes in this city: {{ $tags->sum('city_total_likes') }}<br>Number of tags in this city: {{ $tags->sum('city_n_hashtag') }}<br><br>{{ implode(', ', $tags->pluck('hashtag')->all()) }}').addTo(map);

                            L.circle([{{ $tags->first()->latitude }}, {{ $tags->first()->longitude }}], {
                                color: '#bbbbbb',
                                fillColor: '#dddddd',
                                fillOpacity: 0.4,
                                radius: 10000
                            }).addTo(map);

                            @continue
                        @endforeach
                    @endforeach
        });
    </script>
@endsection