@extends('app')

@section('title', 'Cluster Search Result')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card mt-3">
                <div class="card-body">
                    <h3><b>{{ $text }}</b> -> near cluster n. <b>{{ $ncluster }}</b></h3>
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">Hashtag</th>
                            <th scope="col">Usages</th>
                            <th scope="col">Likes</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($query->groupBy('hashtag') as $hashtag => $data)
                            <tr>
                                <td>{{ $hashtag }}</td>
                                <td>{{ $data->first()->n_hashtag }}</td>
                                <td>{{ $data->first()->total_likes }}</td>
                            </tr>
                            @continue
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
