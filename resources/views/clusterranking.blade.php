@extends('app')

@section('title', 'Cluster Ranking')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card mb-3">
                <div class="card-body">
                    <form method="post" action="{{ route('clustering.clusterWithScoring') }}">
                        @csrf

                        <input type="text" name="phrase" class="form-control" placeholder="Your tweet text">

                        <button class="btn btn-success mt-1" type="submit">Rank</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card mb-3">
                <div class="card-body">
                    <h2 class="card-title text-center">Confusion Matrix</h2>
                    <div class="table-responsive">
                        <table class="table table-bordered" style="font-size: 20px">
                            <tr>
                                <td></td>
                                <td colspan="5">PREDICTED</td>
                            </tr>
                            <tr>
                                <td rowspan="5">
                                    <span style="transform: rotate(180deg);writing-mode: vertical-lr;">REAL</span>
                                </td>
                                <td></td>
                                <td>Up</td>
                                <td>Stable</td>
                                <td>Down</td>
                                <td>Sum</td>
                            </tr>
                            <tr>
                                <td>Up</td>
                                <td>{{ $matrix->confusion_matrix[0][0] }}</td>
                                <td>{{ $matrix->confusion_matrix[0][1] }}</td>
                                <td>{{ $matrix->confusion_matrix[0][2] }}</td>
                                <td>{{ $matrix->confusion_matrix[0][3] }}</td>
                            </tr>
                            <tr>
                                <td>Stable</td>
                                <td>{{ $matrix->confusion_matrix[1][0] }}</td>
                                <td>{{ $matrix->confusion_matrix[1][1] }}</td>
                                <td>{{ $matrix->confusion_matrix[1][2] }}</td>
                                <td>{{ $matrix->confusion_matrix[1][3] }}</td>
                            </tr>
                            <tr>
                                <td>Down<br></td>
                                <td>{{ $matrix->confusion_matrix[2][0] }}</td>
                                <td>{{ $matrix->confusion_matrix[2][1] }}</td>
                                <td>{{ $matrix->confusion_matrix[2][2] }}</td>
                                <td>{{ $matrix->confusion_matrix[2][3] }}</td>
                            </tr>
                            <tr>
                                <td>Sum</td>
                                <td>{{ $matrix->confusion_matrix[3][0] }}</td>
                                <td>{{ $matrix->confusion_matrix[3][1] }}</td>
                                <td>{{ $matrix->confusion_matrix[3][2] }}</td>
                                <td>{{ $matrix->confusion_matrix[3][3] }}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        @php
            $i = 1;
        @endphp
        @foreach($clouds as $cluster => $cow)
            <div class="col-md-4">
                <div class="card mb-3">
                    <div class="card-body">
                        <h5 class="card-title">
                            Cluster #{{$i}}
                            <small class="float-lg-right">Rank trend:
                                <b>{{ ($trends[$cluster] > 0) ? '⬈' : (($trends[$cluster] == 0) ? '=' : '⬊') }}</b>
                            </small>
                        </h5>
                        <h6 class="card-subtitle mb-2">Average score <b>{{ round($ranks[$cluster], 3) }}</b></h6>
                        <div id="tagcloud_{{$cluster}}"></div>
                        <script>
                            $('#tagcloud_{{$cluster}}').jQCloud({!! json_encode($cow) !!}, {
                                height: 200
                            });
                        </script>
                    </div>
                </div>
            </div>
            @php
                $i++;
            @endphp
        @endforeach
    </div>
@endsection