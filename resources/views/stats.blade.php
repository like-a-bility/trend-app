<html>
<head>
    <meta http-equiv="refresh" content="60">
    <title>Stats</title>
</head>
<body>
<h1>
        #cities with tweets: {{ number_format($citiesWithTweets) }} / {{ number_format($totalCities) }} <br>
    #tweets: {{ number_format($totalTweets) }} <br>
    #images with caption: {{ number_format($imagesWithCaption) }} / {{ number_format($imagesTweets) }} <br>
    #twitter users retrieved: {{ number_format($twitterUsers) }} <br>
    #failed jobs: {{ number_format($failedJobs) }} <br>
    #in working jobs: {{ number_format($jobs) }} <br>
</h1>
</body>
</html>