@extends('app')

@section('title', 'Upload')

@section('content')
    <div class="row justify-content-center">
        <div class="col-6">
            <div class="card">
                <div class="card-header">Upload image</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('imagetags.process') }}" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label text-md-right" for="image">Image</label>
                            <div class="col-md-6">
                                <input type="file" class="form-control-file" id="image" name="image">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-success">Send</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
