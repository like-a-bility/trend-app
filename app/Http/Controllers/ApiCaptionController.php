<?php

namespace App\Http\Controllers;

use App\Models\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApiCaptionController extends Controller
{

	public function getCaption()
	{
		DB::beginTransaction();
		// DB::raw('SET TRANSACTION ISOLATION LEVEL REPEATABLE READ');

		$images = Image::where(function ($query) {
			$query->where('caption', null)
				->where('reserved', null);
		})->orWhere(function ($query) {
			$query->where('caption', null)
				->where('reserved', '<', now()->subMinute()->subMinute());
		})->limit(30)->get(['id', 'url']);

		Image::whereIn('id', $images->pluck('id'))->update(['reserved' => now()]);

		DB::commit();

		return response()->json($images, 200, [], JSON_UNESCAPED_SLASHES);
	}

	public function saveCaption(Request $request)
	{
		$json = $request->json();

		$image = Image::findOrFail($json->get('id'));

		$image->caption = $json->get('caption');
		$image->reserved = null;

		if ($json->get('status') === 'DELETE') {
			$status = $image->delete();
		} else {
			$status = $image->save();
		}

		return response()->json(['status' => $status]);
	}
}
