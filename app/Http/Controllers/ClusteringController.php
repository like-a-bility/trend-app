<?php

namespace App\Http\Controllers;

use App\Jobs\ClusterFinder;
use App\Jobs\ConfusionMatrix;
use App\Models\Tweet;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ClusteringController extends Controller
{
    public function clusterStats()
    {
        DB::setDefaultConnection('mysql');
        DB::purge();
        $tweets = $this->getTweets();

        $rankPerDay = $this->getRankPerDay($tweets);
        // chart per day
        $charts = [];
        foreach ($rankPerDay as $cluster => $day) {

            $points = [];
            foreach ($day as $date => $rank) {
                $points[$date] = $rank;
            }

            $occurrences = array_slice($this->getWordCounts($tweets[$cluster]->flatten()->pluck('text')->toArray()), 0, 50);
            $cloud = [];
            foreach ($occurrences as $word => $weight) {
                $cloud[] = [
                    'text' => $word,
                    'weight' => $weight,
                ];
            }

            $countOverTime = [];
            foreach ($tweets[$cluster] as $date => $t) {
                $countOverTime[$date] = $t->count();
            }

            $charts[$cluster] = [$points, $cloud, $countOverTime];
        }

        ksort($charts);
        return view('clusterstats', [
            'charts' => $charts,
        ]);
    }

    private function getTweets()
    {
        $tweets = collect(DB::select('SELECT DISTINCT id, DATE(r.created_at) as date, num_cluster, text, mean_rank from tweets join ranks r on r.tweet_id = id where num_cluster is not null and mean_rank > 0 order by DATE(r.created_at)'));

        $groupedTweets = [];

        $tweets->groupBy('num_cluster')->each(function ($tweets, $numCluster) use (&$groupedTweets) {
            $groupedTweets[$numCluster] = $tweets->groupBy('date');
        });
        return $groupedTweets;
    }

    private function getRankPerDay($tweets)
    {
        $meanRanks = [];
        foreach ($tweets as $num => $cluster) {
            foreach ($cluster as $date => $t) {
                $meanRankForDay = $t->avg('mean_rank');
                $meanRanks[$num][$date] = $meanRankForDay;
            }
        }
        return $meanRanks;
    }

    private function getWordCounts($phrases)
    {
        $commonWords = [
            'a', 'able', 'about', 'above', 'abroad', 'according', 'accordingly', 'across', 'actually', 'adj', 'after', 'afterwards', 'again', 'against', 'ago', 'ahead', 'ain\'t', 'all', 'allow', 'allows', 'almost', 'alone', 'along', 'alongside', 'already', 'also', 'although', 'always', 'am',
            'amid', 'amidst', 'among', 'amongst', 'an', 'and', 'another', 'any', 'anybody', 'anyhow', 'anyone', 'anything', 'anyway', 'anyways', 'anywhere', 'apart', 'appear', 'appreciate', 'appropriate', 'are', 'aren\'t', 'around', 'as', 'a\'s', 'aside', 'ask', 'asking', 'associated', 'at',
            'available', 'away', 'awfully', 'b', 'back', 'backward', 'backwards', 'be', 'became', 'because', 'become', 'becomes', 'becoming', 'been', 'before', 'beforehand', 'begin', 'behind', 'being', 'believe', 'below', 'beside', 'besides', 'best', 'better', 'between', 'beyond', 'both',
            'brief', 'but', 'by', 'c', 'came', 'can', 'cannot', 'cant', 'can\'t', 'caption', 'cause', 'causes', 'certain', 'certainly', 'changes', 'clearly', 'c\'mon', 'co', 'co.', 'com', 'come', 'comes', 'concerning', 'consequently', 'consider', 'considering', 'contain', 'containing',
            'contains', 'corresponding', 'could', 'couldn\'t', 'course', 'c\'s', 'currently', 'd', 'dare', 'daren\'t', 'definitely', 'described', 'despite', 'did', 'didn\'t', 'different', 'directly', 'do', 'does', 'doesn\'t', 'doing', 'done', 'don\'t', 'down', 'downwards', 'during', 'e', 'each',
            'edu', 'eg', 'eight', 'eighty', 'either', 'else', 'elsewhere', 'end', 'ending', 'enough', 'entirely', 'especially', 'et', 'etc', 'even', 'ever', 'evermore', 'every', 'everybody', 'everyone', 'everything', 'everywhere', 'ex', 'exactly', 'example', 'except', 'f', 'fairly', 'far',
            'farther', 'few', 'fewer', 'fifth', 'first', 'five', 'followed', 'following', 'follows', 'for', 'forever', 'former', 'formerly', 'forth', 'forward', 'found', 'four', 'from', 'further', 'furthermore', 'g', 'get', 'gets', 'getting', 'given', 'gives', 'go', 'goes', 'going', 'gone',
            'got', 'gotten', 'greetings', 'h', 'had', 'hadn\'t', 'half', 'happens', 'hardly', 'has', 'hasn\'t', 'have', 'haven\'t', 'having', 'he', 'he\'d', 'he\'ll', 'hello', 'help', 'hence', 'her', 'here', 'hereafter', 'hereby', 'herein', 'here\'s', 'hereupon', 'hers', 'herself', 'he\'s',
            'hi', 'him', 'himself', 'his', 'hither', 'hopefully', 'how', 'howbeit', 'however', 'hundred', 'i', 'i\'d', 'ie', 'if', 'ignored', 'i\'ll', 'i\'m', 'immediate', 'in', 'inasmuch', 'inc', 'inc.', 'indeed', 'indicate', 'indicated', 'indicates', 'inner', 'inside', 'insofar', 'instead',
            'into', 'inward', 'is', 'isn\'t', 'it', 'it\'d', 'it\'ll', 'its', 'it\'s', 'itself', 'i\'ve', 'j', 'just', 'k', 'keep', 'keeps', 'kept', 'know', 'known', 'knows', 'l', 'last', 'lately', 'later', 'latter', 'latterly', 'least', 'less', 'lest', 'let', 'let\'s', 'like', 'liked',
            'likely', 'likewise', 'little', 'look', 'looking', 'looks', 'low', 'lower', 'ltd', 'm', 'made', 'mainly', 'make', 'makes', 'many', 'may', 'maybe', 'mayn\'t', 'me', 'mean', 'meantime', 'meanwhile', 'merely', 'might', 'mightn\'t', 'mine', 'minus', 'miss', 'more', 'moreover', 'most',
            'mostly', 'mr', 'mrs', 'much', 'must', 'mustn\'t', 'my', 'myself', 'n', 'name', 'namely', 'nd', 'near', 'nearly', 'necessary', 'need', 'needn\'t', 'needs', 'neither', 'never', 'neverf', 'neverless', 'nevertheless', 'new', 'next', 'nine', 'ninety', 'no', 'nobody', 'non', 'none',
            'nonetheless', 'noone', 'no-one', 'nor', 'normally', 'not', 'nothing', 'notwithstanding', 'novel', 'now', 'nowhere', 'o', 'obviously', 'of', 'off', 'often', 'oh', 'ok', 'okay', 'old', 'on', 'once', 'one', 'ones', 'one\'s', 'only', 'onto', 'opposite', 'or', 'other', 'others',
            'otherwise', 'ought', 'oughtn\'t', 'our', 'ours', 'ourselves', 'out', 'outside', 'over', 'overall', 'own', 'p', 'particular', 'particularly', 'past', 'per', 'perhaps', 'placed', 'please', 'plus', 'possible', 'presumably', 'probably', 'provided', 'provides', 'q', 'que', 'quite', 'qv',
            'r', 'rather', 'rd', 're', 'really', 'reasonably', 'recent', 'recently', 'regarding', 'regardless', 'regards', 'relatively', 'respectively', 'right', 'round', 's', 'said', 'same', 'saw', 'say', 'saying', 'says', 'second', 'secondly', 'see', 'seeing', 'seem', 'seemed', 'seeming',
            'seems', 'seen', 'self', 'selves', 'sensible', 'sent', 'serious', 'seriously', 'seven', 'several', 'shall', 'shan\'t', 'she', 'she\'d', 'she\'ll', 'she\'s', 'should', 'shouldn\'t', 'since', 'six', 'so', 'some', 'somebody', 'someday', 'somehow', 'someone', 'something', 'sometime',
            'sometimes', 'somewhat', 'somewhere', 'soon', 'sorry', 'specified', 'specify', 'specifying', 'still', 'sub', 'such', 'sup', 'sure', 't', 'take', 'taken', 'taking', 'tell', 'tends', 'th', 'than', 'thank', 'thanks', 'thanx', 'that', 'that\'ll', 'thats', 'that\'s', 'that\'ve', 'the',
            'their', 'theirs', 'them', 'themselves', 'then', 'thence', 'there', 'thereafter', 'thereby', 'there\'d', 'therefore', 'therein', 'there\'ll', 'there\'re', 'theres', 'there\'s', 'thereupon', 'there\'ve', 'these', 'they', 'they\'d', 'they\'ll', 'they\'re', 'they\'ve', 'thing',
            'things', 'think', 'third', 'thirty', 'this', 'thorough', 'thoroughly', 'those', 'though', 'three', 'through', 'throughout', 'thru', 'thus', 'till', 'to', 'together', 'too', 'took', 'toward', 'towards', 'tried', 'tries', 'truly', 'try', 'trying', 't\'s', 'twice', 'two', 'u', 'un',
            'under', 'underneath', 'undoing', 'unfortunately', 'unless', 'unlike', 'unlikely', 'until', 'unto', 'up', 'upon', 'upwards', 'us', 'use', 'used', 'useful', 'uses', 'using', 'usually', 'v', 'value', 'various', 'versus', 'very', 'via', 'viz', 'vs', 'w', 'want', 'wants', 'was',
            'wasn\'t', 'way', 'we', 'we\'d', 'welcome', 'well', 'we\'ll', 'went', 'were', 'we\'re', 'weren\'t', 'we\'ve', 'what', 'whatever', 'what\'ll', 'what\'s', 'what\'ve', 'when', 'whence', 'whenever', 'where', 'whereafter', 'whereas', 'whereby', 'wherein', 'where\'s', 'whereupon',
            'wherever', 'whether', 'which', 'whichever', 'while', 'whilst', 'whither', 'who', 'who\'d', 'whoever', 'whole', 'who\'ll', 'whom', 'whomever', 'who\'s', 'whose', 'why', 'will', 'willing', 'wish', 'with', 'within', 'without', 'wonder', 'won\'t', 'would', 'wouldn\'t', 'x', 'y', 'yes',
            'yet', 'you', 'you\'d', 'you\'ll', 'your', 'you\'re', 'yours', 'yourself', 'yourselves', 'you\'ve', 'z', 'zero',
        ];
        $pattern = '/\b('.implode('|', $commonWords).')\b/i';

        $counts = [];
        foreach ($phrases as $phrase) {
            $phrase = strtolower($phrase);
            $phrase = preg_replace("@(https?://([-\w\.]+[-\w])+(:\d+)?(/([\w/_\.#-]*(\?\S+)?[^\.\s])?)?)@", '', $phrase);
            $words = explode(' ', preg_replace($pattern, '', $phrase));
            foreach ($words as $word) {
                $word = preg_replace("#[^a-zA-Z]#", '', $word);
                if ($word === '') {
                    continue;
                }
                isset($counts[$word]) ? $counts[$word] += 1 : $counts[$word] = 1;
            }
        }
        arsort($counts);
        return $counts;
    }

    /**
     * @param  Request  $request
     * @throws Exception
     */
    public function clusterSearch(Request $request)
    {
        $text = $request->get('phrase');

        $job = new ClusterFinder($text);
        $this->dispatchNow($job);

        if (!$job->ok()) {
            throw new Exception($job->getErrorOutput());
        }

        $cluster = $job->getCluster();

        $ids = Tweet::select('id')->where('num_cluster', $cluster)->get()->pluck('id');

        $tweets = DB::table('tweets')
            ->join('hashtags_tweets', 'tweets.id', '=', 'hashtags_tweets.tweet_id')
            ->join('hashtags', 'hashtags.id', '=', 'hashtags_tweets.hashtag_id')
            ->select([
                'hashtags.id',
                'hashtag',
                DB::raw('COUNT(hashtag) AS n_hashtag'),
                DB::raw('SUM(likes) AS total_likes'),
            ])
            ->whereIn('tweets.id', $ids)
            ->groupBy('hashtags.id', 'hashtag')
            ->orderByDesc('n_hashtag')
            ->orderByDesc('likes');

        return view('clusteresult', [
            'query' => $tweets->limit(50)->get(),
            'ncluster' => $cluster,
            'text' => $text,
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws Exception
     */
    public function clusterSearchPage()
    {
        $ranks = collect(DB::select('select distinct AVG(mean_rank) as rank, num_cluster from tweets join ranks r on r.tweet_id = tweets.id where num_cluster is not null group by num_cluster'))
            ->mapWithKeys(function ($item) {
                return [$item->num_cluster => $item->rank];
            });

        $tweets = collect(DB::select('select distinct num_cluster, text from tweets join ranks r on r.tweet_id = id where num_cluster is not null and mean_rank > 0'))
            ->groupBy('num_cluster')
            ->sortByDesc(function ($e, $k) use (&$ranks) {
                return $ranks[$k];
            });

        $clouds = [];
        foreach ($tweets as $cluster => $tweetsByDay) {
            $occurrences = array_slice($this->getWordCounts($tweetsByDay->pluck('text')->toArray()), 0, 30);
            foreach ($occurrences as $word => $weight) {
                $clouds[$cluster][] = [
                    'text' => $word,
                    'weight' => $weight,
                ];
            }
        }

        $trends = collect($this->getTweets())->mapWithKeys(function ($dates, $c) {
            $avgs = $dates->map(function ($elements) {
                return $elements->avg('mean_rank');
            })->values();
            $avgcopy = collect($avgs);
            $avgcopy->shift();
            $avgs->pop();
            return [
                $c => $avgs->map(function ($e, $k) use (&$avgcopy) {
                    return $avgcopy[$k] - $e;
                })->sum(),
            ];
        });

        $job = new ConfusionMatrix();
        $this->dispatchNow($job);

        if (!$job->ok()) {
            throw new Exception($job->getErrorOutput());
        }

        $matrix = json_decode($job->getOutput());

        return view('clusterranking', [
            'ranks' => $ranks,
            'clouds' => $clouds,
            'trends' => $trends,
            'matrix' => $matrix
        ]);
    }

    public function clusterWithScoring(Request $request)
    {
        $text = $request->get('phrase');

        $job = new ClusterFinder($text);
        $this->dispatchNow($job);

        if (!$job->ok()) {
            throw new Exception($job->getErrorOutput());
        }
        $ranks = collect(DB::select('select distinct AVG(mean_rank) as rank, num_cluster from tweets join ranks r on r.tweet_id = tweets.id where num_cluster is not null group by num_cluster'))
            ->mapWithKeys(function ($item) {
                return [$item->num_cluster => $item->rank];
            });

        $tweets = collect(DB::select('select distinct num_cluster, text from tweets join ranks r on r.tweet_id = id where num_cluster is not null and mean_rank > 0 order by mean_rank'))
            ->groupBy('num_cluster')
            ->sortBy(function ($e, $k) use (&$ranks) {
                return $ranks[$k];
            });

        $clouds = [];
        foreach ($tweets as $cluster => $tweetsByDay) {
            $occurrences = array_slice($this->getWordCounts($tweetsByDay->pluck('text')->toArray()), 0, 25);
            foreach ($occurrences as $word => $weight) {
                $clouds[$cluster][] = [
                    'text' => $word,
                    'weight' => $weight,
                ];
            }
        }

        $cluster = $job->getCluster();
        $max = $ranks->values()->max();
        $min = $ranks->values()->min();

        $rank = $ranks[$cluster];
        $calc = $rank - ($job->getDistance() / 100) * $rank;
        $trendMeter = 100 * (($calc - $min) / ($max - $min));

        $exampleTweets = $tweets[$cluster]->take(12);

        return view('clusterscoring', [
            'distance' => $calc,
            'score' => $trendMeter,
            'tweets' => $tweets,
            'progressTrend' => $job->getGrow(),
            'clouds' => $clouds,
            'fallInto' => $cluster,
            'text' => $text,
            'ranks' => $ranks,
            'examples' => $exampleTweets,
            'max' => $max,
            'min' => $min,
        ]);
    }
}
