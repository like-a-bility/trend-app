<?php

namespace App\Http\Controllers;

use App\Jobs\CaptionOneImage;
use App\Models\Hashtag;
use App\Models\Image;
use App\Models\Tweet;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use DonatelloZa\RakePlus\RakePlus;
use Whoops\Exception\ErrorException;

class ImageTagsController extends Controller
{

	public function uploadImage()
	{
		return view('upload');
	}

	/**
	 * @param Request $request
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 * @throws Exception
	 */
	public function getHashtags(Request $request)
	{
	    DB::setDefaultConnection('mysql2');
	    DB::purge();
		$img = $request->file('image')->storePublicly('public' . DIRECTORY_SEPARATOR . 'images');

		$job = new CaptionOneImage(storage_path('app' . DIRECTORY_SEPARATOR . $img));
		$this->dispatchNow($job);

		if (!$job->ok()) {
			throw new Exception($job->getErrorOutput());
		}

		$caption = $job->getCaption();

		$imageIds = Image::select('tweet_id')
			->selectFulltextScore($caption)
			->whereFulltextCaption($caption)
			->limit(50)
			->get();

		$ids = collect($imageIds->pluck('tweet_id'));

		$tweetIds = Tweet::select('id')
			->selectFulltextScore($caption)
			->whereFulltextText($caption)
			->limit(50)
			->get();

		$ids = $ids->merge($tweetIds->pluck('id'));

		$tweets = DB::table('tweets')
			->join('hashtags_tweets', 'tweets.id', '=', 'hashtags_tweets.tweet_id')
			->join('hashtags', 'hashtags.id', '=', 'hashtags_tweets.hashtag_id')
			->select([
				'hashtags.id',
				DB::raw('COUNT(hashtag) AS n_hashtag'),
				DB::raw('SUM(likes) AS total_likes'),
			])
			->whereIn('tweets.id', $ids)
			->groupBy('hashtags.id', 'hashtag');
			


		$result = DB::table('tweets')
			->join('hashtags_tweets', 'tweets.id', '=', 'hashtags_tweets.tweet_id')
			->join('hashtags', 'hashtags.id', '=', 'hashtags_tweets.hashtag_id')
			->join('cities', 'tweets.city_id', '=', 'cities.id')
			->join('countries', 'cities.country_id', '=', 'countries.id')
			->select([
				'hashtags.id',
				'hashtag',
				'latitude',
				'longitude',
				DB::raw('cities.name AS city'),
				DB::raw('countries.name AS country'),
				DB::raw('COUNT(hashtag) AS city_n_hashtag'),
				DB::raw('SUM(likes) AS city_total_likes'),
				'n_hashtag',
				'total_likes',
			])
			->distinct()
			->joinSub($tweets, 'tweets_with_count', function ($join) {
				$join->on('hashtags_tweets.hashtag_id', '=', 'tweets_with_count.id');
			})
			->whereIn('tweets.id', $ids)
			->groupBy('hashtags.id', 'hashtag','country', 'city', 'latitude', 'longitude', 'n_hashtag', 'total_likes')
			->orderByDesc('n_hashtag')
			->orderByDesc('likes')
			->get();

		$content = 'data: ' . mime_content_type(storage_path('app' . DIRECTORY_SEPARATOR . $img)) . ';base64,' . base64_encode(Storage::get($img));
		Storage::delete($img);

		$sums = collect();
		foreach ($result->groupBy(['country', 'city']) as $cities) {
			foreach ($cities as $city) {
				$sums->push($city->sum('city_total_likes'));
			}
		}


		return view('hashtagmap', [
			'image' => $content,
			'caption' => $this->normalizeCaption($caption),
			'query' => $result,
			'max_likes_cities' => $sums->max()
		]);
	}

	private function normalizeCaption($caption)
	{
		$caption = str_replace('<start>', '', $caption);
		$caption = str_replace('<end>', '', $caption);
		$caption = str_replace('.', '', $caption);
		$caption = ucfirst(trim($caption));

		return "$caption.";
	}
}
