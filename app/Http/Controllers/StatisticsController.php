<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\Image;
use App\Models\Tweet;
use App\Models\TwitterUser;
use Illuminate\Http\Request;
use Illuminate\Queue\Jobs\Job;
use Illuminate\Support\Facades\DB;

class StatisticsController extends Controller
{

    private function getStatistics()
    {
        $citiesWithTweets = Tweet::distinct('city_id')->count('city_id');
        $totalCities = City::count();

        $totalTweets = Tweet::count('id');

        $imagesTweets = Image::count();
        $imagesWithCaption = Image::where('caption', '!=', null)->count();

        $twitterUsers = TwitterUser::count();

        $failedJobs = DB::table('failed_jobs')->count();
        $jobs = DB::table('jobs')->count();

        return ['citiesWithTweets' => $citiesWithTweets,
            'totalCities' => $totalCities,
            'totalTweets' => $totalTweets,
            'imagesTweets' => $imagesTweets,
            'imagesWithCaption' => $imagesWithCaption,
            'twitterUsers' => $twitterUsers,
            'failedJobs' => $failedJobs,
            'jobs' => $jobs];
    }

    public function getFunnyStatistics()
    {
        $array = $this->getStatistics();
        return view('funnystats', $array);
    }

    public function getNotFunnyStatistics()
    {
        $array = $this->getStatistics();
        return view('stats', $array);

    }
}
