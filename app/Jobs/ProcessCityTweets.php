<?php

namespace App\Jobs;

use App\Mention;
use App\Models\City;
use App\Models\Hashtag;
use App\Models\Image;
use App\Models\Tweet;
use App\Models\TwitterUser;
use App\Reply;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use SergiX44\Scraper\TwitterScraper;

class ProcessCityTweets implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	/**
	 * @var City
	 */
	private $city;

	/**
	 * @var string
	 */
	private $radius;

	/**
	 * @var \DateTime
	 */
	private $start;

	/**
	 * @var \DateTime
	 */
	private $end;

	/**
	 * Create a new job instance.
	 *
	 * @param City $city
	 * @param string $radius
	 * @param \DateTime $start
	 * @param \DateTime $end
	 */
	public function __construct(City $city, string $radius, \DateTime $start, \DateTime $end)
	{
		$this->city = $city;
		$this->radius = $radius;
		$this->start = $start;
		$this->end = $end;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 */
	public function handle()
	{
		try {
			TwitterScraper::make()
				->search("-filter:nativeretweets filter:hashtags filter:images geocode:{$this->city->latitude},{$this->city->longitude},{$this->radius}", $this->start, $this->end)
				->setLang('en')
				->setChunkSize(200)
				->save(function (array $tweets, $count) {
					echo "[city={$this->city->name}] Found {$count} tweets. Saving..." . PHP_EOL;
					foreach ($tweets as $tweet) {
						DB::transaction(function () use (&$tweet) {
							$this->saveOneTweet($tweet);
						}, 3);
					}
					echo "[city={$this->city->name}] Saving completed." . PHP_EOL;
				}, true)
				->run();
		} catch (Exception $e) {
			$this->fail($e);
		}
	}

	public function saveOneTweet(array $scrapedTweet)
	{
		TwitterUser::updateOrCreate(
			['id' => $scrapedTweet['user_id']],
			[
				'username' => $scrapedTweet['user_name'],
				'fullname' => $scrapedTweet['user_fullname'],
			]
		);

		$tweet = Tweet::firstOrCreate(
			['id' => $scrapedTweet['tweetId']],
			[
				'user_id' => $scrapedTweet['user_id'],
				'city_id' => $this->city->id,
				'url' => $scrapedTweet['url'],
				'likes' => $scrapedTweet['likes'],
				'replies' => $scrapedTweet['replies'],
				'retweets' => $scrapedTweet['retweets'],
				'tweet_date' => $scrapedTweet['datetime'],
				'text' => $scrapedTweet['text'],
			]
		);

		$ids = [];
		foreach ($scrapedTweet['hashtags'] as $hashtag) {
			$savedHashtag = Hashtag::firstOrCreate([
				'hashtag' => $hashtag
			]);

			$ids[] = $savedHashtag->id;
		}
		$tweet->hashtags()->sync($ids);

		foreach ($scrapedTweet['images'] as $image) {
			Image::firstOrCreate([
				'tweet_id' => $scrapedTweet['tweetId'],
				'url' => $image
			]);
		}

		foreach ($scrapedTweet['mentions'] as $userId => $username) {
			TwitterUser::updateOrCreate(
				['id' => $userId],
				[
					'username' => $username
				]
			);

			Mention::firstOrCreate([
				'user_id' => $userId,
				'tweet_id' => $scrapedTweet['tweetId'],
			]);
		}

		if ($scrapedTweet['reply_to'] !== null) {
			foreach ($scrapedTweet['reply_to'] as $userId => $username) {
				TwitterUser::updateOrCreate(
					['id' => $userId],
					[
						'username' => $username
					]
				);

				Reply::firstOrCreate([
					'user_id' => $userId,
					'tweet_id' => $scrapedTweet['tweetId'],
				]);
			}
		}
	}
}
