<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class CaptionOneImage implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	const RECOGNIZER_PATH = 'recognizer-v2/';

	/**
	 * @var string
	 */
	private $imagePath;

	/**
	 * @var string
	 */
	private $caption;

	/**
	 * @var bool
	 */
	private $ok;

	/**
	 * @var bool|string
	 */
	private $errorOutput;

	/**
	 * Create a new job instance.
	 *
	 * @param string $path
	 */
	public function __construct(string $path)
	{
		$this->imagePath = $path;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$cwd = config('app.recognizer_path', base_path(self::RECOGNIZER_PATH));

		if (is_file("{$cwd}.venv\Scripts\python.exe")) {
			$exec = "{$cwd}.venv\Scripts\python.exe";
			$opts = ['SYSTEMROOT' => 'C:\WINDOWS'];
		} else {
			$exec = "{$cwd}.venv/bin/python";
			$opts = [];
		}

		$recognizer = new Process([$exec, 'caption_cli.py', '--img', $this->imagePath], $cwd, array_merge([
			'LC_ALL' => 'C.UTF-8',
			'LANG' => 'C.UTF-8',
		], $opts));

		//$recognizer->setTty(true);
		$recognizer->run();

		$this->ok = $recognizer->isSuccessful();

		if (!$recognizer->isSuccessful()) {
			$this->fail(new ProcessFailedException($recognizer));
		}
		$this->caption = $recognizer->getOutput();
		$this->errorOutput = $recognizer->getErrorOutput();
	}

	/**
	 * @return string
	 */
	public function getCaption(): ?string
	{
		return trim($this->caption);
	}

	/**
	 * @return bool|null
	 */
	public function ok(): ?bool
	{
		return $this->ok;
	}

	/**
	 * @return bool|string
	 */
	public function getErrorOutput()
	{
		return $this->errorOutput;
	}
}
