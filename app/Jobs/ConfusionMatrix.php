<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class ConfusionMatrix implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    const CLUSTER_FINDER_PATH = 'spacy/';

    /**
     * @var bool|string
     */
    private $errorOutput;
    /**
     * @var bool
     */
    private $ok;


    private $result;

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $cwd = config('app.cluster_finder_path', base_path(self::CLUSTER_FINDER_PATH));

        if (is_file("{$cwd}.venv\Scripts\python.exe")) {
            $exec = "{$cwd}.venv\Scripts\python.exe";
            $opts = ['SYSTEMROOT' => 'C:\WINDOWS'];
        } else {
            $exec = "{$cwd}.venv/bin/python";
            $opts = [];
        }
        //'/home/sergio/.local/share/virtualenvs/spacy-mNuv4C9m/bin/python'
        $finder = new Process([$exec, 'confusion_matrix_with_normalization.py'], $cwd, array_merge([
            'LC_ALL' => 'C.UTF-8',
            'LANG' => 'C.UTF-8',
        ], $opts));

        //$recognizer->setTty(true);
        $finder->run();

        $this->ok = $finder->isSuccessful();

        if (!$finder->isSuccessful()) {
            $this->fail(new ProcessFailedException($finder));
        }
        $this->result = trim($finder->getOutput());
        $this->errorOutput = $finder->getErrorOutput();
    }

    public function getOutput()
    {
        return $this->result;
    }

    /**
     * @return bool
     */
    public function ok()
    {
        return $this->ok;
    }

    /**
     * @return bool|string
     */
    public function getErrorOutput()
    {
        return $this->errorOutput;
    }
}
