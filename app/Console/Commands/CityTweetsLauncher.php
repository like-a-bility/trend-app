<?php

namespace App\Console\Commands;

use App\Jobs\ProcessCityTweets;
use App\Models\City;
use Illuminate\Console\Command;

class CityTweetsLauncher extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'scrape:all {start} {end} {radius=10km}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Launch the process city tweets jobs';

	/**
	 * @var \DateTime
	 */
	private $end;

	/**
	 * @var \DateTime
	 */
	private $start;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 * @throws \Exception
	 */
	public function handle()
	{
		$this->start = new \DateTime($this->argument('start'));
		$this->end = new \DateTime($this->argument('end'));

		City::chunk(100, function ($cities) {    // take only 100 cities per iteration
			foreach ($cities as $city) {           // and than create a job for each city
				ProcessCityTweets::dispatch($city, $this->argument('radius'), $this->start, $this->end);
			}
		});
	}
}
