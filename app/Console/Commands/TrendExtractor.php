<?php

namespace App\Console\Commands;

use App\Models\City;
use DG\Twitter\Twitter;
use Illuminate\Console\Command;

class TrendExtractor extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'trend:extractor';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Extract the trends';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 * @throws \DG\Twitter\Exception
	 */
	public function handle()
	{
		$twitter = new Twitter(config('twitter.consumer_key'),
			config('twitter.consumer_secret'),
			config('twitter.access_token'),
			config('twitter.access_token_secret')
		);

		$this->line(print_r($twitter->search(['q'=>' ', 'geocode'=>'40,4251, -74,021']), true));

	}
}
