<?php

namespace App\Console\Commands;

use App\Jobs\ProcessCityLimitedTweets;
use App\Jobs\ProcessCityTweets;
use App\Models\City;
use Illuminate\Console\Command;

class QueryOneCityLauncher extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'scrape:city {city} {start} {end} {radius=10km}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Launch scrape on one specified city';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 * @throws \Exception
	 */
	public function handle()
	{
		$this->start = new \DateTime($this->argument('start'));
		$this->end = new \DateTime($this->argument('end'));

		$city = City::where('name', $this->argument('city'))->first();

		ProcessCityLimitedTweets::dispatch($city, $this->argument('radius'), $this->start, $this->end);
	}
}
