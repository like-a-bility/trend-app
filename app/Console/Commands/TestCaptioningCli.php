<?php

namespace App\Console\Commands;

use App\Jobs\CaptionOneImage;
use Illuminate\Console\Command;

class TestCaptioningCli extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'test:captioning {path}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Test captioning cli';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 */
	public function handle()
	{
		$job = new CaptionOneImage($this->argument('path'));
		dispatch_now($job);
		if (!$job->ok()) {
	    	$this->line($job->getErrorOutput());
		}
		$this->line($job->getCaption());
	}
}
