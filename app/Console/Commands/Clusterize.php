<?php

namespace App\Console\Commands;

use App\Models\Tweet;
use Illuminate\Console\Command;
use Phpml\Clustering\KMeans;

class Clusterize extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'clusterize';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$tweets = Tweet::select('mean_vec', 'id')->get();

		$samples = [];
		foreach ($tweets as $tweet) {
			$samples[(string)$tweet->id] = [$tweet->mean_vec];
		}

		$kmeans = new KMeans(20, KMeans::INIT_KMEANS_PLUS_PLUS);
		$cluster = $kmeans->cluster($samples);

		file_put_contents('out.json', json_encode($cluster), JSON_PRETTY_PRINT);
	}
}
