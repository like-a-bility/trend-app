<?php

namespace App\Console;

use App\Console\Commands\CityNotFoundLauncher;
use App\Console\Commands\CityTweetsLauncher;
use App\Console\Commands\BuildCaptions;
use App\Console\Commands\QueryOneCityLauncher;
use App\Console\Commands\TestCaptioningCli;
use App\Console\Commands\TrendExtractor;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [
		TrendExtractor::class,
		CityTweetsLauncher::class,
		QueryOneCityLauncher::class,
		CityNotFoundLauncher::class,
		TestCaptioningCli::class,
	];

	/**
	 * Define the application's command schedule.
	 *
	 * @param \Illuminate\Console\Scheduling\Schedule $schedule
	 * @return void
	 */
	protected function schedule(Schedule $schedule)
	{
		// $schedule->command('inspire')
		//          ->hourly();
	}

	/**
	 * Register the commands for the application.
	 *
	 * @return void
	 */
	protected function commands()
	{
		$this->load(__DIR__ . '/Commands');

		require base_path('routes/console.php');
	}
}
