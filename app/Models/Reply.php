<?php

namespace App;

use App\Models\TwitterUser;
use Illuminate\Database\Eloquent\Model;

class Reply extends Model
{
	public $timestamps = false;

	protected $table = 'replies';

	protected $fillable = [
		'user_id',
		'tweet_id',
	];

	public function user()
	{
		return $this->belongsTo(TwitterUser::class, 'user_id');
	}
}
