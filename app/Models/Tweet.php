<?php

namespace App\Models;

use App\Mention;
use App\Reply;
use Illuminate\Database\Eloquent\Model;

class Tweet extends Model
{

	public $incrementing = false;

	protected $dates = [
		'tweet_date',
	];

	protected $fillable = [
		'id',
		'user_id',
		'city_id',
		'url',
		'likes',
		'replies',
		'retweets',
		'tweet_date',
		'text',
		'vectorization',
		'num_cluster'
	];

	public function city()
	{
		return $this->belongsTo(City::class);
	}

	public function user()
	{
		return $this->belongsTo(TwitterUser::class, 'user_id');
	}

	public function images()
	{
		return $this->hasMany(Image::class);
	}

	public function hashtags()
	{
		return $this->belongsToMany(Hashtag::class, 'hashtags_tweets');
	}

	public function replies()
	{
		return $this->hasMany(Reply::class);
	}

	public function mentions()
	{
		return $this->hasMany(Mention::class);
	}

	public function ranks()
	{
		return $this->hasMany(Rank::class, 'tweet_id');
	}

/*	public function keywords()
	{
		return $this->belongsToMany(Keyword::class, 'keywords_tweets');
	}*/

	public function scopeSelectFullTextScore($query, $term)
	{
		return $query->selectRaw('MATCH(text) AGAINST(? IN NATURAL LANGUAGE MODE) AS score', [$term]);
	}

	public function scopeWhereFullTextText($query, $term, $min = 0)
	{
		return $query->whereRaw('MATCH(text) AGAINST(? IN NATURAL LANGUAGE MODE) > ?', [$term, $min]);
	}
}
