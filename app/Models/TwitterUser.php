<?php

namespace App\Models;

use App\Mention;
use App\Reply;
use Illuminate\Database\Eloquent\Model;

class TwitterUser extends Model
{
	protected $table = 'twitter_users';

	public $incrementing = false;

	protected $fillable = [
		'id',
		'username',
		'fullname',
		'followers',
	];

	public function tweets()
	{
		return $this->hasMany(Tweet::class, 'user_id');
	}

	public function mentions()
	{
		return $this->hasMany(Mention::class, 'user_id');
	}

	public function replies()
	{
		return $this->hasMany(Reply::class, 'user_id');
	}
}
