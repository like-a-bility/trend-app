<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
	public $timestamps = false;

	protected $fillable = [
		'code',
		'name'
	];

	public function cities()
	{
		return $this->hasMany(City::class);
	}

	public function regions()
	{
		return $this->hasMany(Region::class);
	}
}
