<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
	protected $table = 'cities';

	public $timestamps = false;

	protected $fillable = [
		'region_id',
		'country_id',
		'name',
		'type',
		'latitude',
		'longitude',
		'population'
	];

	public function country()
	{
		return $this->belongsTo(Country::class);
	}

	public function region()
	{
		return $this->belongsTo(Region::class);
	}

	public function tweets()
	{
		return $this->hasMany(Tweet::class);
	}
}
