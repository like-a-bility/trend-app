<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rank extends Model
{

	public $timestamps = false;
	public $incrementing = false;

	protected $fillable = [
		'tweet_id',
		'likes',
		'retweets',
		'rank_likes',
		'mean_rank',
		'rank_retweets'
	];

	public function tweet(){
		return $this->belongsTo(Tweet::class, 'tweet_id');
	}

}
