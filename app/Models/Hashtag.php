<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Hashtag extends Model
{
	public $timestamps = false;

	protected $fillable = [
		'hashtag'
	];

	public function tweets()
	{
		return $this->belongsToMany(Tweet::class, 'hashtags_tweets');
	}
}
