<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Image extends Model
{
	use SoftDeletes;

	public $timestamps = false;

	protected $fillable = [
		'url',
		'tweet_id',
		'caption'
	];

	protected $dates = [
		'reserved'
	];

	public function tweet()
	{
		return $this->belongsTo(Tweet::class);
	}

	public function scopeSelectFullTextScore($query, $term)
	{
		return $query->selectRaw('MATCH(caption) AGAINST(? IN NATURAL LANGUAGE MODE) AS score', [$term]);
	}

	public function scopeWhereFullTextCaption($query, $term, $min = 0)
	{
		return $query->whereRaw('MATCH(caption) AGAINST(? IN NATURAL LANGUAGE MODE) > ?', [$term, $min]);
	}
}
