<?php

use App\Http\Controllers\ApiCaptionController;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::get('caption', [ApiCaptionController::class, 'getCaption'])->name('caption.get');
Route::post('caption/save', [ApiCaptionController::class, 'saveCaption'])->name('caption.save');