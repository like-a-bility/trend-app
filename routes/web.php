<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/', 'HomeController@root')->name('root');
Route::get('stats', 'StatisticsController@getNotFunnyStatistics');
Route::get('stats/funny', 'StatisticsController@getFunnyStatistics');

Route::get('search', 'ImageTagsController@uploadImage')->name('imagetags.upload');
Route::post('search', 'ImageTagsController@getHashtags')->name('imagetags.process');


Route::get('clustering', 'ClusteringController@clusterStats')->name('clustering.clusterstats');
Route::post('clustering/search', 'ClusteringController@clusterSearch')->name('clustering.search');

Route::get('clustering/ranking', 'ClusteringController@clusterSearchPage')->name('clustering.clusterSearchPage');
Route::post('clustering/ranking', 'ClusteringController@clusterWithScoring')->name('clustering.clusterWithScoring');