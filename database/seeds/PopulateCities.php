<?php

use App\Models\City;
use App\Models\Country;
use App\Models\Region;
use Illuminate\Database\Seeder;

class PopulateCities extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$json = json_decode(file_get_contents(__DIR__ . '/cities.json'));
		foreach ($json as $index => $city) {

			$country = Country::firstOrCreate([
				'code' => $city->iso2,
				'name' => $city->country
			]);

			if ($city->admin !== '') {
				$region = Region::firstOrCreate([
					'name' => $city->admin,
					'country_id' => $country->id
				]);
			} else {
				$region = null;
			}

			City::create([
				'region_id' => ($region === null ? $region : $region->id),
				'country_id' => $country->id,
				'name' => $city->city,
				'type' => ($city->capital === '' ? null : $city->capital),
				'latitude' => $city->lat,
				'longitude' => $city->lng,
				'population' => ($city->population === '' ? null : max($city->population, 0))
			]);

			echo $index . PHP_EOL;
		}

	}
}
