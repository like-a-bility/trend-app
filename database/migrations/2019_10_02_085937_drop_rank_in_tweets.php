<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropRankInTweets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tweets', function (Blueprint $table) {
	        $table->dropColumn('rank_likes');
	        $table->dropColumn('rank_retweets');
	        $table->dropColumn('mean_vec');
	        $table->unsignedInteger('num_cluster')->nullable();
	        $table->unsignedInteger('likes')->nullable()->change();
	        $table->unsignedInteger('retweets')->nullable()->change();
	        $table->unsignedInteger('replies')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tweets', function (Blueprint $table) {
	        $table->float('rank_likes')->nullable();
	        $table->float('rank_retweets')->nullable();
	        $table->float('mean_vec')->nullable();
	        $table->dropColumn('num_cluster');
	        $table->unsignedInteger('likes')->change();
	        $table->unsignedInteger('retweets')->change();
	        $table->unsignedInteger('replies')->change();
        });
    }
}
