<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReplies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('replies', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('tweet_id');

            $table->foreign('tweet_id')->references('id')->on('tweets')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('twitter_users')->onUpdate('cascade')->onDelete('cascade');
            $table->primary(['user_id', 'tweet_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('replies');
    }
}
