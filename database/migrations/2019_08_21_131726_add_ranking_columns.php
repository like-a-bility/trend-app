<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRankingColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('tweets', function (Blueprint $table) {
		    $table->float('rank_likes')->nullable();
		    $table->float('rank_retweets')->nullable();
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tweets', function (Blueprint $table){
            $table->dropColumn('rank_likes');
            $table->dropColumn('rank_retweets');
        });
    }
}
