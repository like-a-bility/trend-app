<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTweetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tweets', function (Blueprint $table) {
            $table->unsignedBigInteger('id');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('city_id')->nullable();
            $table->string('url');
            $table->unsignedInteger('likes');
            $table->unsignedInteger('retweets');
            $table->unsignedInteger('replies');
            $table->dateTime('tweet_date');
            $table->text('text');
            $table->timestamps(); //updateAt and createAt

            $table->primary('id');
            $table->foreign('user_id')->references('id')->on('twitter_users')->onUpdate('cascade')->onDelete('set NULL');
            $table->foreign('city_id')->references('id')->on('cities')->onUpdate('cascade')->onDelete('set NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tweets');
    }
}
