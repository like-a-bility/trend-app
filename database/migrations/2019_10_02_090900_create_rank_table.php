<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRankTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ranks', function (Blueprint $table) {
	        $table->unsignedBigInteger('tweet_id');
	        $table->unsignedInteger('likes');
	        $table->unsignedInteger('retweets');
	        $table->float('rank_likes');
	        $table->float('rank_retweets');
	        $table->float('mean_rank');
            $table->timestamp('created_at')->useCurrent();

	        $table->primary(['tweet_id', 'created_at']);
	        $table->foreign('tweet_id')->references('id')->on('tweets')->onUpdate('cascade')->onDelete('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rank');
    }
}
