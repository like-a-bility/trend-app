<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWordVecAndMeanVec extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::table('tweets', function (Blueprint $table) {
			$table->float('mean_vec')->nullable();
			$table->json('vectorization')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tweets', function (Blueprint $table){
			$table->dropColumn('vectorization');
			$table->dropColumn('mean_vec');
		});
	}
}
