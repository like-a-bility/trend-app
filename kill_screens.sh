#!/bin/bash

screen -ls "$1" | (
  IFS=$(printf '\t');
  sed "s/^$IFS//" |
  while read -r name stuff; do
      screen -S "$name" -X quit
  done
)
