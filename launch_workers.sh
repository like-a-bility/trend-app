#!/bin/bash

for i in {1..10}; do
	screen -dmS "worker_${i}" "${1}" artisan queue:work --sleep=1 --tries=3 --timeout=0
	sleep 3
	echo "${i}"
done
